"use strict";

let image = [
  {
    picture: "./img/1.jpg",
  },
  {
    picture: "./img/2.jpg",
  },
  {
    picture: "./img/3.JPG",
  },
  {
    picture: "./img/4.png",
  },
];

const content = document.querySelector(".images-wrapper");
const container = document.querySelector(".container");
let interval = setInterval(slider, 3000);
let counter = 0;

container.addEventListener("click", (e) => {
  if (e.target.classList.contains("play")) {
    console.log(1);
    interval = setInterval(slider, 3000);
  } else if (e.target.classList.contains("stop")) {
    console.log(2);
    clearInterval(interval);
  }
});

function slider() {
  counter++;
  if (counter > image.length - 1) {
    counter = 0;
  }
  drawPhoto(counter);
}

function drawPhoto(counter = 0) {
  content.innerHTML = `<img src=${image[counter].picture} alt="image of game" class="image-to-show" width="500" height="300"/>`;
}
drawPhoto();
